package eu.petur.trains;

import org.testng.annotations.Test;

import java.io.FileNotFoundException;
import java.net.URL;

import static org.testng.Assert.*;

public class NetworkBuilderTest {

  @Test
  public void testConstructor() throws Exception {
    new NetworkBuilder();
  }

  @Test
  public void testBuild() throws Exception {
    final URL networkConfig = this.getClass().getResource("/Configurations/Valid/Network1");
    NetworkBuilder.build(networkConfig);
  }
  
  @Test
  public void testNetworkWithComments() throws Exception {
    final URL networkConfig = this.getClass().getResource("/Configurations/Valid/Network3");
    NetworkBuilder.build(networkConfig);
  }

  @Test(expectedExceptions = NullPointerException.class)
  public void testBuild_nullConfiguration() throws Exception{
    final URL networkConfig = this.getClass().getResource("/Configurations/NON_EXISTING");
    NetworkBuilder.build(networkConfig);
  }

  @Test(expectedExceptions = IllegalStateException.class)
  public void testBuild_RejectsConnectionToAndFromSameTrack() throws Exception {
    final URL networkConnection = this.getClass().getResource("/Configurations/Invalid/Network_ConnectionAtSamePoint");
    NetworkBuilder.build(networkConnection);
  }
  /*
  //TODO: Not sure these are the right exception classes the parser would throw at different types of invalid input
  @Test(expectedExceptions = IllegalStateException.class)
  public void testBuild_RejectsNetworkWithMissingStationName() throws Exception {
    final URL networkConnection = this.getClass().getResource("/Configurations/Invalid/Network_MissingStationName");
    NetworkBuilder.build(networkConnection);
  }
  
  @Test(expectedExceptions = IllegalStateException.class)
  public void testBuild_RejectsNetworkWithMissingEndIdentifier() throws Exception {
    final URL networkConnection = this.getClass().getResource("/Configurations/Invalid/Network_MissingIdentifierEnd");
    NetworkBuilder.build(networkConnection);
  }
  
  @Test(expectedExceptions = IllegalStateException.class)
  public void testBuild_RejectsNetworkWithMissingConnectionIdentifiers() throws Exception {
    final URL networkConnection = this.getClass().getResource("/Configurations/Invalid/Network_MissingIdentifiersConnection");
    NetworkBuilder.build(networkConnection);
  }

  @Test(expectedExceptions = IllegalStateException.class)
  public void testBuild_RejectsNetworkWithDuplicates() throws Exception {
    final URL networkConnection = this.getClass().getResource("/Configurations/Invalid/Network_DuplicateStation");
    NetworkBuilder.build(networkConnection);
  }

  @Test(expectedExceptions = IllegalStateException.class)
  public void testBuild_RejectsEmptyInput() throws Exception {
    final URL networkConnection = this.getClass().getResource("/Configurations/Invalid/Network_EmptyInput");
    NetworkBuilder.build(networkConnection);
  }
  
  @Test(expectedExceptions = IllegalStateException.class)
  public void testBuild_RejectsNonsensicalInput() throws Exception {
    final URL networkConnection = this.getClass().getResource("/Configurations/Invalid/Network_NonsensicalInput");
    NetworkBuilder.build(networkConnection);
  }*/
}