package eu.petur.trains;

import org.testng.annotations.Test;

import static org.testng.Assert.*;

public class EndTest {

  @Test(expectedExceptions = IllegalStateException.class)
  public void testCanOnlyConnectToOnePoint() throws Exception {
    final End end = new End();
    end.connectTo(new Point("Test 1"));
    end.connectTo(new Point("Test 2")); // Should throw IllegalStateException.
  }

  @Test(expectedExceptions = NullPointerException.class)
  public void testNoNullPoints() throws Exception{
    final End end = new End();
    end.connectTo(null);
  }

  @Test(expectedExceptions = IllegalArgumentException.class)
  public void testCanNotConnectToPointWhichIsConnectedToAnotherEnd() throws Exception {
    final End differentEnd = new End();
    final Point point = new Point("TestPoint");
    differentEnd.connectTo(point);
    point.connectTrack(differentEnd);

    final End end = new End();
    end.connectTo(point);
  }
}