package eu.petur.trains;

import org.apache.commons.collections4.ListUtils;
import org.apache.commons.lang3.StringUtils;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.List;

import org.testng.Assert;

public class PointTest {

  Point validPoint;
  final String validPointId = "ValidTestPoint";
  @BeforeMethod
  public void setUp() throws Exception {
    validPoint = new Point(validPointId);
  }

  @Test(expectedExceptions = NullPointerException.class)
  public void testPointWithNullId() throws Exception {
    new Point(null);
  }

  @Test(expectedExceptions = IllegalArgumentException.class)
  public void testPointWithEmptyId() throws Exception {
    new Point("");
  }

  @Test
  public void testConnectTrack() throws Exception {
    final Track track = new Straight();
    validPoint.connectTrack(track);
    Assert.assertTrue(validPoint.getConnectedTracks().contains(track), "Failed to connect track to point.");
    Assert.assertTrue(track.connectedPoints() == 0, "connectTrack should not interconnect with tracks.");
  }

  @Test(expectedExceptions = IllegalArgumentException.class)
  public void testConnectTMoreThanThreeTracks() throws Exception {
    for (int i = 0; i < 5; i++) {
      final Track track = new Straight();
      validPoint.connectTrack(track);
    }
  }

  @Test(expectedExceptions = IllegalStateException.class)
  public void testConnectTrack_cannotConnectSameTrackTwice() {
    final Track track = new Straight();
    validPoint.connectTrack(track);
    validPoint.connectTrack(track);
  }

  @Test
  public void testDisconnectTrack() throws Exception {
    final Track straight = new Straight();
    validPoint.connectTrack(straight);
    validPoint.disconnectTrack(straight);
    Assert.assertTrue(validPoint.connectionCount() == 0, "Failed to disconnect a track.");
  }

  @Test
  public void testHasStationConnected() throws Exception {
    final Track station = new Station("S", validPoint);
    validPoint.connectTrack(station);
    Assert.assertTrue(validPoint.hasStationConnected());
  }

  @Test
  public void test_noConnectionsOnInit() throws Exception {
    Assert.assertTrue(validPoint.getConnectedTracks().size() == 0, "Initial track count should be zero.");
    Assert.assertTrue(validPoint.connectionCount() == 0, "Expected no connections.");
  }

  @Test
  public void testConnectionCount() throws Exception {
    final int expected = 3;
    for (int i = 0; i < expected; i++)
      validPoint.connectTrack(new Straight());
    Assert.assertTrue(validPoint.connectionCount() == expected, "Expected " + expected + " connections.");

  }

  @Test
  public void testGetId() throws Exception {
    Assert.assertTrue(validPoint.getId().equals(validPointId), "Failed to set id");
  }

  @Test
  public void testGetConnectedTracks() throws Exception {
    final int expected = 3;
    List<Track> tracks = new ArrayList<Track>(3);
    for (int trackId = 0; trackId < expected; trackId++) {
      final Track straight = new Straight();
      tracks.add(straight);
      validPoint.connectTrack(straight);
    }
    Assert.assertTrue(ListUtils.isEqualList(tracks, validPoint.getConnectedTracks()), "Not all tracks were stored.");
  }

  @Test
  public void testEquals() throws Exception {
    Assert.assertTrue(!validPoint.equals(""), "Point can not be equal to empty string.");
    final Point differentPoint = new Point("NotEqual");
    Assert.assertTrue(!validPoint.equals(differentPoint), "Points should not be equal.");
    Assert.assertTrue(validPoint.equals(validPoint), "Points should be equal.");
    Assert.assertTrue(validPoint.equals(new Point(validPointId)), "Points should be equal.");

  }

  @Test
  public void testToString() throws Exception {
    // This is a development method. Ensure it returns something.
    Assert.assertTrue(!StringUtils.isEmpty(validPoint.toString()), "toString() not implemented.");
  }
}