package eu.petur.trains;

import org.testng.Assert;
import org.testng.annotations.Test;

import java.io.FileNotFoundException;
import java.net.URL;

public class TrainRouteBuilderTest {

  @Test
  public void testTrainRouteBuilder() throws Exception {
    TrainRouteBuilder trb = new TrainRouteBuilder();
  }

  @Test
  public void testValidTrivial() throws Exception {
    final RailSystem railSystem = new RailSystem(TrainRouteBuilderTest.class.getClass().getResource("/Configurations/Valid/Network1"),
                                                 TrainRouteBuilderTest.class.getClass().getResource("/Configurations/Valid/TrainRoutes1"));
  }

  @Test
  public void testValidNonTrivial() throws Exception {
    final RailSystem railSystem = new RailSystem(TrainRouteBuilderTest.class.getClass().getResource("/Configurations/Valid/Network2"),
                                                 TrainRouteBuilderTest.class.getClass().getResource("/Configurations/Valid/TrainRoutes2"));
  }

  @Test(expectedExceptions = IllegalStateException.class)
  public void testInvalidRouteForNetwork() throws Exception {
    final RailSystem railSystem = new RailSystem(TrainRouteBuilderTest.class.getClass().getResource("/Configurations/Valid/Network1"),
                                                 TrainRouteBuilderTest.class.getClass().getResource("/Configurations/Valid/TrainRoutes2"));
  }

  @Test(expectedExceptions = IllegalStateException.class)
  public void testTrainCanNotChangeDirectionWithoutStopping() throws Exception {
    final RailSystem railSystem = new RailSystem(TrainRouteBuilderTest.class.getClass().getResource("/Configurations/Valid/Network4_StraightNetwork"),
                                                 TrainRouteBuilderTest.class.getClass().getResource("/Configurations/Invalid/Route_ChangeDirectionWithoutStopping"));
  }

  @Test(expectedExceptions = NullPointerException.class)
  public void testBuildWithNullConfiguration() throws Exception {
    TrainRouteBuilder.build(null);
  }

  @Test(expectedExceptions = FileNotFoundException.class)
  public void testBuild_reportsFileNotFound() throws Exception {
    final URL invalid = new URL("http://fileNotFound/invalidFile");
    TrainRouteBuilder.build(invalid);
  }

  @Test
  public void testBuild_validRouteWithThreePoints() throws Exception {
    final Route route = TrainRouteBuilder.build(TrainRouteBuilderTest.class.getClass().getResource("/Configurations/Valid/Route_3Points"));
    Assert.assertTrue(route.waypoints().size() == 3);
  }
}