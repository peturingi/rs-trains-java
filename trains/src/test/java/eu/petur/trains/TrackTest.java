package eu.petur.trains;

import org.apache.commons.collections4.ListUtils;
import org.apache.commons.lang3.StringUtils;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.LinkedList;
import java.util.List;


public class TrackTest {

  // Testing abstract class.
  class ConcreteTrack extends Track {
  }

  Track track;

  @BeforeMethod
  public void setUp() throws Exception {
    track = new ConcreteTrack();
  }

  @Test
  public void testConnectTo() throws Exception {
    List<Point> points = addTenPointsToTrack();
    Assert.assertEquals(track.connectedToPoints(), points);
  }

  private List<Point> addTenPointsToTrack() {
    final int pointCount = 10;
    final List<Point> points = new LinkedList<Point>();
    for (int pointId = 0; pointId < pointCount; pointId++) {
      Point p = new Point(String.valueOf(pointId));
      points.add(p);
      track.connectTo(p);
    }
    return points;
  }

  @Test(expectedExceptions = NullPointerException.class)
  public void testConnectedPoints_rejectsNullPoint() {
    track.connectTo(null);
  }

  @Test
  public void testConnectedPoints() throws Exception {
    addTenPointsToTrack();
    Assert.assertTrue(track.connectedPoints() == 10);
  }

  @Test
  public void testDisconnectFrom() throws Exception {
    List<Point> points = addTenPointsToTrack();
    List<Point> toDisconnect = points.subList(0, points.size() / 2);
    for (Point p: toDisconnect) {
      track.disconnectFrom(p);
    }
    Assert.assertTrue(track.connectedPoints() == points.size() - toDisconnect.size());
    Assert.assertTrue(track.connectedToPoints().containsAll(ListUtils.subtract(points, toDisconnect)));
  }

  @Test(expectedExceptions = NullPointerException.class)
  public void testDisconnectFrom_rejectsNull() throws Exception {
    track.disconnectFrom(null);
  }

  @Test(expectedExceptions = IllegalArgumentException.class)
  public void testDisonnectFrom_rejectsUnknownPoints() {
    final Point unknown = new Point("Unknown Point");
    track.disconnectFrom(unknown);
  }

  @Test
  public void testLeads() throws Exception {
    final Point from = new Point("FromId");
    final Point to = new Point("ToId");
    track.connectTo(from);
    track.connectTo(to);
    Assert.assertTrue(track.leads(from.getId(), to.getId()));
    Assert.assertFalse(track.leads("UnknownPoint", "UnknownPoint"));
  }

  @Test(expectedExceptions = NullPointerException.class)
  public void testLeads_rejectsNullInFirstParameter() throws Exception {
    track.leads(null, "Invalid Point ID");
  }
  @Test(expectedExceptions = NullPointerException.class)
  public void testLeads_rejectsNullInSecondParameter() throws Exception {
    track.leads("Invalid Point ID", null);
  }
  @Test(expectedExceptions = IllegalArgumentException.class)
  public void testLeads_rejectsEmptyFirstParameter() throws Exception {
    track.leads("", "Invalid Point ID");
  }
  @Test(expectedExceptions = IllegalArgumentException.class)
  public void testLeads_rejectsEmptySecondParameter() throws Exception {
    track.leads("Invalid Point ID", "");
  }

  @Test
  public void testToString() throws Exception {
    Assert.assertTrue(StringUtils.isNotEmpty(track.toString()));
  }
}