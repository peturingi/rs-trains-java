package eu.petur.trains;

import org.apache.commons.lang3.StringUtils;
import org.testng.Assert;
import org.testng.annotations.Test;
import org.testng.collections.CollectionUtils;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

import static org.testng.Assert.*;

public class SwitchTest {

  @Test
  public void testConstructor() throws Exception {
    final Point primary = new Point("Primary Test");
    final Point alt1 = new Point("Alt1 Test");
    final Point alt2 = new Point("Alt2 Test");
    final Switch sw = new Switch(primary, alt1, alt2);
    Assert.assertEquals(sw.getPrimary(), primary);
    final List<Point> alts = sw.connectedToPoints();
    Assert.assertEquals(alts.get(0), alt1);
    Assert.assertEquals(alts.get(1), alt2);
  }

  @Test(expectedExceptions = NullPointerException.class)
  public void testConstructor_primaryNotNull() throws Exception {
    new Switch(null, new Point("Alt1 Test"), new Point("Alt2 Test"));
  }

  @Test(expectedExceptions = NullPointerException.class)
  public void testConstructor_alt1NotNull() throws Exception {
    new Switch(new Point("Primary Test"), null, new Point("Alt2 Test"));
  }

  @Test(expectedExceptions = NullPointerException.class)
  public void testConstructor_alt2NotNull() throws Exception {
    new Switch(new Point("Primary Test"), new Point("Alt1 Test"), null);
  }

  @Test(expectedExceptions = IllegalArgumentException.class)
  public void testConstructor_parametersMustBeUnique() throws Exception {
    new Switch(new Point("Test"), new Point("Test"), new Point("Test"));
  }

  @Test
  public void testGetPrimary() throws Exception {
    final Switch sw = new Switch(new Point("Primary Test"), new Point("Alt1 Test"), new Point("Alt2 Test"));
    Assert.assertTrue(sw.getPrimary().equals(new Point("Primary Test")));
  }

  @Test
  public void testLeads() throws Exception {
    final Switch sw = new Switch(new Point("A"), new Point("B"), new Point("C"));
    /* Primary to alts and back. */
    Assert.assertTrue(sw.leads("A", "B"));
    Assert.assertTrue(sw.leads("A", "C"));
    Assert.assertTrue(sw.leads("B", "A"));
    Assert.assertTrue(sw.leads("C", "A"));
    /* Between alts. */
    Assert.assertFalse(sw.leads("B", "C"));
    Assert.assertFalse(sw.leads("C", "B"));
  }

  @Test
  public void testConnectedToPoints() throws Exception {
    final List<Point> points = new LinkedList<Point>(Arrays.asList(new Point("A"),
                                                                   new Point("B"),
                                                                   new Point("C")));
    final Switch sw = new Switch(points.get(0), points.get(1), points.get(2));
    int equalElements = 0;
    for (Point received: sw.connectedToPoints())
      for (Point local: points)
        if (received.equals(local))
          equalElements++;
    Assert.assertEquals(equalElements, 3);
  }

  @Test
  public void testToString() throws Exception {
    Assert.assertTrue(StringUtils.isNotEmpty(new Switch(new Point("A"), new Point("B"), new Point("C")).toString()));
  }
}