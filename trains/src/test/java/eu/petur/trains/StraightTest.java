package eu.petur.trains;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import org.testng.Assert;

public class StraightTest {
  Straight straight;

  @BeforeMethod
  public void setUp() throws Exception {
    straight = new Straight();
  }

  @Test
  public void testConnectTo() throws Exception {
    final Point a = new Point("Test Point");
    final Point b = new Point("Test Point 2");
    straight.connectTo(a);
    straight.connectTo(b);
    Assert.assertTrue(isConnectedTo(a) && isConnectedTo(b), "Failed to connect to point.");
  }

  private boolean isConnectedTo(final Point point) {
    return straight.connectedToPoints().contains(point);
  }

  @Test(expectedExceptions = IllegalStateException.class)
  public void testCanNotConnectToThreePoints() throws Exception {
    final int pointCount = 3;
    for (int i = 0; i < pointCount; i++) {
      straight.connectTo(new Point(String.valueOf(i)));
    }
  }

  @Test(expectedExceptions = IllegalStateException.class)
  public void testCanNotTwoTimesToSamePoint() throws Exception {
    final Point a = new Point("Test Point");
    straight.connectTo(a);
    straight.connectTo(a);
  }
}