package eu.petur.trains;

import org.apache.commons.lang3.StringUtils;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import org.testng.Assert;


public class StationTest {
  Station station;
  Point location;
  String stationId;
  String locationId;
  @BeforeMethod
  public void setUp() throws Exception {
    stationId = "Test Station";
    locationId = "Test Point";
    location = new Point(locationId);
    station = new Station(stationId, location);
  }

  @Test
  public void testIsAtPointWithId() throws Exception {
    Assert.assertTrue(station.isAtPointWithId(locationId));
  }

  @Test(expectedExceptions = NullPointerException.class)
  public void testIsAtPoint_rejectsNull() throws Exception {
    station.isAtPointWithId(null);
  }

  @Test(expectedExceptions = IllegalArgumentException.class)
  public void testIsAtPoint_rejectEmpty() throws Exception {
    station.isAtPointWithId("");
  }

  @Test
  public void testIsAtPoint_no() throws Exception {
    Assert.assertFalse(station.isAtPointWithId("unknown point"));
  }

  @Test
  public void testStation() throws Exception {
    Station s = new Station(stationId, location);
  }

  @Test(expectedExceptions = NullPointerException.class)
  public void testStation_rejectNullId() throws Exception {
    new Station(null, location);
  }

  @Test(expectedExceptions = IllegalArgumentException.class)
  public void testStation_rejectEmptyId() throws Exception {
    new Station("", location);
  }

  @Test(expectedExceptions = NullPointerException.class)
  public void testStation_rejectNullLocation() throws Exception {
    new Station(stationId, null);
  }

  @Test(expectedExceptions = IllegalArgumentException.class)
  public void testCanNotConnectToPointWhichIsConnectedToAnotherStation() throws Exception {
    final Point point = new Point("TestPoint");
    final Station differentStation = new Station("stationName", point);
    differentStation.connectTo(point);
    point.connectTrack(differentStation);

    final Station station = new Station("stationName2", point);
    station.connectTo(point);
    point.connectTrack(station);
  }

  @Test
  public void testToString() throws Exception {
    Assert.assertTrue(StringUtils.isNotEmpty(station.toString()));
  }

}