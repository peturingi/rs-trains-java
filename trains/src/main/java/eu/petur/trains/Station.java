package eu.petur.trains;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.Validate;

/**
 * <code>Station</code> is the class representing a train station.
 * A train station is a track on which a train can stop.
 * Stations are special in that both their ends are connected to the same point.
 */
public final class Station extends Straight {

  /**
   * The name of this station.
   */
  private final String name;

  /**
   * Class constructor specifying a name and a point.
   *
   * @param name name of this station.
   * @param point point to which this station is connected.
   * @throws NullPointerException <code>name</code> was null.
   * @throws IllegalArgumentException <code>name</code> was empty.
   * @throws NullPointerException <code>point</code> was null.
   */
  public Station(final String name, final Point point) {
    Validate.notNull(point, "Point was null.");
    Validate.notNull(name, "Name was empty.");
    Validate.isTrue(StringUtils.isNotEmpty(name), "Name was null or empty.");
    this.name = name;
    connectTo(point);
    isStation = true;
  }

  /**
   * Evaluate whether this station is connected to a given point.
   *
   * @param id id of a point
   * @return true if this station is connected to a point with id <code>id</code>; false otherwise.
   * @throws NullPointerException id is null.
   * @throws IllegalArgumentException id is empty.
   */
  public boolean isAtPointWithId(final String id) {
    Validate.notNull(id, "The ID cannot be null.");
    Validate.isTrue(StringUtils.isNotEmpty(id), "The ID has to contain at least one character.");
    for (Point p: connectedToPoints())
      if (p.getId().equals(id))
        return true;
    return false;
  }

  /**
   * Intended only for debugging.
   *
   * @return this stations name and the two points to which it is connected.
   */
  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    for (Point p: connectedToPoints())
      sb.append(p.toString()).append(" ");
    return name + " " + sb.toString();
  }

  /**
   * Connect this station track to a point.
   * @param point the point this track is to connect to.
   * @throws IllegalStateException tried to connect to more than 2 points.
   * @throws IllegalArgumentException if the <code>track</code> was not connected correctly.
   */
  @Override
  public void connectTo(final Point point) {
    if (connectedPoints() == 2)
      throw new IllegalStateException("Can not connect more than 2 points.");
    final int oldPointsCount = this.points.size();
    points.add(point);
    final int newPointsCount = this.points.size();
    Validate.isTrue(newPointsCount == oldPointsCount + 1, "Connection was not added correctly");
  }
}
