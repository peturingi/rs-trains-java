grammar TrainRoutes;

routes : (id|stop)*;

stop : 'STOP' id;

id : STRING;

STRING : ([a-z]|[A-Z]|[0-9])+ ;

WS  :   [ \t\n\r]+ -> skip;
