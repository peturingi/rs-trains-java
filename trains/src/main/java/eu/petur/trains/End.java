package eu.petur.trains;

import org.apache.commons.lang3.Validate;

/**
 * <code>End</code> is the class representing an <i>end track</i>.
 * An end track is a <i>track</i> with a connection on one side that does end.
 * <code>End</code> connects to a <code>Point</code>.
 *
 * @see Point
 * @see Track
 */
public final class End extends Track {

  /**
   * Connects this end track to a point.
   *
   * <p><b>Preconditions</b><br>
   *   This end is not connected to a point.<br>
   *   <code>point</code> does not connect to an end track.
   *   </p>
   * @param point the point to connect to.
   * @throws NullPointerException <code>point</code> was null.
   * @throws IllegalArgumentException <code>point</code> already holds a connection to an end.
   * @throws IllegalStateException this end track has already been connected to a point.
   */
  @Override
  public void connectTo(final Point point) {
    Validate.notNull(point, "Point cannot value cannot be null.");
    for (Track track: point.getConnectedTracks())
      Validate.isTrue(!(track instanceof End), "Point already holds a connection to an end track.");
    if (connectedPoints() != 0)
      throw new IllegalStateException("This end track has already been connected to a point.");

    super.connectTo(point);
  }
}
