package eu.petur.trains;

import eu.petur.trains.gen.Networks.NetworksBaseVisitor;
import eu.petur.trains.gen.Networks.NetworksParser;
import org.antlr.v4.runtime.misc.NotNull;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.Validate;

import java.util.*;

/**
 * <code>NetworkBuilderVisitor</code> build a railway network from a parse tree.
 *
 * @see End
 * @see Point
 * @see Station
 * @see Straight
 * @see Switch
 * @see Track
 */
public final class NetworkBuilderVisitor extends NetworksBaseVisitor<String> {

  /**
   * Maps an identifier to a point.
   */
  private Map<String, Point> points = new HashMap<String, Point>();

  /**
   * Tracks in the network this builder is building.
   */
  private Set<Track> tracks = new HashSet<Track>();

  /**
   * Straight tracks in the network this builder is building.
   */
  private Set<Straight> straights = new HashSet<Straight>();

  /**
   * Get the tracks in this network.
   * The list is empty if the network contains no tracks.
   *
   * @return tracks in this network.
   */
  public Set<Track> getTracks() {
    return tracks;
  }

  /**
   * <p><b>This method is for use by an ANTLR generated parser; do not invoke this method directly.</b></p>
   * Visit the <i>station</i> non-terminal and its children.
   */
  @Override
  public String visitStation(@NotNull NetworksParser.StationContext ctx) {
    /* TODO
    Ensure that a station with the given identifier does not already exist.
    If one exists, make sure an error is reported to the user.
    This can only happen if a STAT with the same id was declared two times in Network configuration. */

    /** Gets the point at which this station is to be placed. */
    final String pointIdStationPlacement = ctx.id().getText();
    final Point point = getPoint(pointIdStationPlacement);

    /**
     * Create the station represented by this non-terminal and interconnect it with the point at its location.
     */
    final String stationId = ctx.STRING().getText();
    final Station station = new Station(stationId, point);
    interConnect(station, point);
    tracks.add(station);

    return ""; // Not used.
  }

  /**
   * Interconnect track and point.
   *
   * @param track <code>track</code to be connected to <code>point</code>.
   * @param point <code>point</code> to be connected to <code>track</code>.
   * @throws IllegalArgumentException <code>track</code> was null.
   * @throws IllegalArgumentException <code>point</code> was null.
   */
  private void interConnect(final Track track, final Point point) {
    Validate.notNull(track, "Track was null.");
    Validate.notNull(point, "Point was null.");
    track.connectTo(point);
    point.connectTrack(track);
  }

  /**
   * <p><b>This method is for use by an ANTLR generated parser; do not invoke this method directly.</b></p>
   * Visit the <i>conn</i> non-terminal and its children.
   *
   * @throws IllegalStateException <code>CONN id id</code> where <code>id == id</code> was found in configuration file.
   */
  @Override
  public String visitConnect(@NotNull NetworksParser.ConnectContext ctx) {
    Validate.isTrue(ctx.id().size() == 2, "A connection must be between two points only.");

    final Straight straight = new Straight();

    final Point pointA = getPoint(ctx.id(0).STRING().getText());
    interConnect(straight, pointA);

    final Point pointB = getPoint(ctx.id(1).STRING().getText());
    interConnect(straight, pointB);

    /**
     * A straight track can not lead to and from the same point.
     * Can only be caused by an invalid configuration file; Unrecoverable.
     */
    if (pointA.getId().equals(pointB.getId()))
      throw new IllegalStateException("CONN " + pointA.getId() + " " + pointB.getId() +" : Invalid! points must differ.");

    tracks.add(straight);
    straights.add(straight);

    return ""; // Not used.
  }

  /**
   * Get a point with identifier <code>id</code> from known points.
   * If no point is known with identifier <code>id</code>, a new point is created and stored among known points.
   * @param id id of the point to get or create.
   * @return a point with id.
   */
  private Point getPoint(final String id) {
    Validate.notNull(id, "ID value cannot be null.");
    final Point point = points.get(id) != null ? points.get(id) : new Point(id);
    points.put(id, point);
    return point;
  }

  /**
   * <p><b>This method is for use by an ANTLR generated parser; do not invoke this method directly.</b></p>
   * Visit the <i>end</i> non-terminal and its children.
   *
   * @throws IllegalStateException the visited end is to connect to a point which has already been connected to an end.
   */
  @Override
  public String visitEnd(@NotNull NetworksParser.EndContext ctx) {
    /**
     * Connect the end to a point.
     */
    final Point point = getPoint(ctx.id().getText());
    final End end = new End();
    interConnect(end, point);

    /**
     * Two end tracks can not be connected to the same point.
     * Can only be caused by an invalid configuration file; Unrecoverable.
     */
    for (Track track: point.getConnectedTracks())
      if (track instanceof End)
        if (track != end)
          if (track.connectedToPoints().get(0).equals(point))
            throw new IllegalStateException("Two ends can not be placed at the same point: " + point.getId());

    points.put(point.getId(), point);
    tracks.add(end);

    return ""; // Not used.
  }

  /**
   * <p><b>This method is for use by an ANTLR generated parser; do not invoke this method directly.</b></p>
   * Visit the <i>network</i> non-terminal and its children.
   */
  @Override
  public String visitNetworks(@NotNull NetworksParser.NetworksContext ctx) {
    // Build station tracks
    for (NetworksParser.StationContext s: ctx.station())
      visitStation(s);

    // Build end tracks
    for (NetworksParser.EndContext e: ctx.end())
      visitEnd(e);

    // Build straight tracks
    for (NetworksParser.ConnectContext c: ctx.connect())
      visitConnect(c);

    // Some straight tracks represent switches.
    // Build switches out of them.
    buildSwitches();

    /* Ensure all points are connected to tracks. */
    for (Map.Entry<String, Point> entry : points.entrySet())
      if (entry.getValue().connectionCount() == 0)
        throw new IllegalStateException("Point " + entry.getValue().getId() + " is not connected to any tracks.");

    return "";
  }

  /**
   * Convert points with three straights, to a switch.
   *
   * @throws IllegalStateException Found a switch which is not fully connected.
   */
  private void buildSwitches() {
    /* A point which connects three straight, is a point at the center of a track switch.
     * Get a list of all such points in the network. */
    Map<Point, Integer> pointFrequency = new HashMap<Point, Integer>();
    for (Straight s: straights) {
      List<Point> connectedToPoints = s.connectedToPoints();
      for (Point p: connectedToPoints) {
        if (pointFrequency.containsKey(p)) {
          pointFrequency.put(p, pointFrequency.get(p) + 1);
        } else
          pointFrequency.put(p, 1);
      }
    }
    List<Point> pointsWithThreeConnections = new ArrayList<Point>(); // This is the list.
    for (Point p: pointFrequency.keySet())
      if (pointFrequency.get(p) == 3)
        pointsWithThreeConnections.add(p);

    /* Convert the three straight tracks leading to the each point in the above list, into a switch.
     * The DSL used to construct networks dictates that a switch is to be indicated such:
     * CONN a b
     * CONN b c
     * CONN b d
     *
     * b appears in two columns in the above example, as such it is the switches center.
     * The row, containing the only b in a column, represents a 'primary' branch.
     * The two rows, containing the b's which appear in the same column, represent alternative branches.
     *
     * A train can only travel between a primary branch and an alternative branch.
     * A train can NOT travel directly between alternative branches.
     *
     * note unrelated to the code:
     * A train can travel indirectly between the alternative branches:
     * This can be done by marking the track connected to the primary branch as stoppable,
     * then make the train stop there and then roll back into the other alternative branch.
     */
    for (Point switchCenter: pointsWithThreeConnections) {
      final List<Track> switchTracks = switchCenter.getConnectedTracks();
      if (switchTracks.size() != 3)
        throw new IllegalStateException("A switch should have connected three tracks.");
      final List<Point> trackAPoints = switchTracks.get(0).connectedToPoints();
      /* naming: rxcy row x column y */
      Point r1c1 = trackAPoints.get(0);
      Point r1c2 = trackAPoints.get(1);
      final List<Point> trackBPoints = switchTracks.get(1).connectedToPoints();
      Point r2c1 = trackBPoints.get(0);
      Point r2c2 = trackBPoints.get(1);
      final List<Point> trackCPoints = switchTracks.get(2).connectedToPoints();
      Point r3c1 = trackCPoints.get(0);
      Point r3c2 = trackCPoints.get(1);

      // Find primary point.
      // It is the unique element in either first or second column.
      final Set<Point> first = new HashSet<Point>(Arrays.asList(r1c1, r2c1, r3c1));
      final Set<Point> second = new HashSet<Point>(Arrays.asList(r1c2, r2c2, r3c2));
      /* c0 c1 c2
       * CONN a b
       * CONN b c
       * CONN b d
       * Sets containing column 1 and 2 must have (unordered count) 2 and 3 elements.
       */
      if (!setsHaveUnorderedSizes(first, second, 2, 3))
        throw new IllegalStateException();

      /* Center is an element which belongs to both sets. */
      final Collection<Point> intersection = CollectionUtils.intersection(first, second);
      /* Primary is the element in the smaller set, which is not the center. */
      final Collection<Point> primaryCollection = CollectionUtils.subtract(first.size() < second.size() ? first : second, intersection);
      if (primaryCollection.size() != 1)
        throw new IllegalStateException("Expected one primary.");
      /* Branches are elements in the larger list, which are not the center. */
      final Collection<Point> branches = CollectionUtils.subtract(first.size() < second.size() ? second : first, intersection);
      if (branches.size() != 2)
        throw new IllegalStateException("Expected two branches.");

      /* Disconnect the interconnection of tracks leading to the center of switch and their connected points. */
      while (switchCenter.getConnectedTracks().size() > 0) {
        final Track track = switchCenter.getConnectedTracks().get(0);
        this.straights.remove(track);
        this.tracks.remove(track);
        while (track.connectedToPoints().size() > 0) {
          final Point pt = track.connectedToPoints().get(0);
          pt.disconnectTrack(track);
          track.disconnectFrom(pt);
        }
      }
      // Delete center point of switch
      points.remove(switchCenter.getId(), switchCenter);

      /* Interconnect the new switch with the points previously connected to the straight tracks we deleted. */
      // Connect switch to points.
      final Iterator<Point> branchIterator = branches.iterator();
      final Point primary = primaryCollection.iterator().next();
      final Switch sw = new Switch(primary, branchIterator.next(), branchIterator.next());
      // Connect points to switch.
      primary.connectTrack(sw);
      for (Point branch: branches)
        branch.connectTrack(sw);

      // Switch has been constructed. Add it to list of tracks.
      tracks.add(sw);
    }
  }

  /**
   * Check if the two integers match the sets sizes.
   * The order of parameters is not taken into account.
   *
   * @param a first set
   * @param b second set
   * @param x first integer
   * @param y second integer
   * @return true if a contains x elements and b contains y elements, or the other way around; false otherwise.
   * @throws IllegalArgumentException either set is null or either integer is not strictly positive.
   */
  private boolean setsHaveUnorderedSizes(final Set a, final Set b, final int x, final int y) {
    Validate.notNull(a, "The set cannot be null.");
    Validate.notNull(b, "The set cannot be null.");
    Validate.isTrue(x >= 0 && x >= 0, "The x and y values cannot be negative.");
    return ((a.size() == x && b.size() == y) || ( a.size() == y && b.size() == x));
  }

}
