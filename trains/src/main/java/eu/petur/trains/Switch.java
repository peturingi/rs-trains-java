package eu.petur.trains;

import org.apache.commons.lang3.Validate;

import java.util.Arrays;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;

/**
 * Switch connects to alternative tracks to a route from which either one can be chosen.
 */
public final class Switch extends Track {

  /**
   * The point from which the alternatives can be chosen.
   */
  private Point primary;

  /**
   * Class constructor with all of the points a switch will connect.
   *
   * @param primary from this point, either of the alts can be visited.
   * @param alt1 from this point, only the primary can be visited.
   * @param alt2 from this point, only the primary can be visited.
   * @throws NullPointerException any of the points were null.
   */
  public Switch(final Point primary, final Point alt1, final Point alt2) {
    Validate.notNull(primary, "The point cannot be null.");
    Validate.notNull(alt1, "The point cannot be null.");
    Validate.notNull(alt2, "The point cannot be null.");

    final HashSet<String> pointsIds = new HashSet<String>();
    for (Point p: Arrays.asList(primary, alt1, alt2))
      pointsIds.add(p.getId());
    if (pointsIds.size() != 3)
      throw new IllegalArgumentException("A switch can not be reflexive. All points must have a unique name.");

    this.primary = primary;
    connectTo(alt1);
    connectTo(alt2);
  }

  /**
   * Get this switches primary point.
   *
   * @return this switches primary point.
   */
  public Point getPrimary() {
    return primary;
  }

  @Override
  public boolean leads(final String fromId, final String toId) {
    // Switch has a direction. Its primary leads to branches.
    // Branches never lead to each other.
    // A branch only leads to fromId or toId if its primary is either fromId or toId.
    if (!primary.getId().equals(fromId) && !primary.getId().equals(toId))
      return false;

    // Primary is either fromId or toId. Find the one it is not.
    final String nonPrimary = primary.getId().equals(fromId) ? toId : fromId;

    // Check if the non-primary is at a branch.
    for (Point p: connectedToPoints())
      if (p.getId().equals(nonPrimary))
        return true;

    return false;
  }

  @Override
  public List<Point> connectedToPoints() {
    final List<Point> connectedPoints = new LinkedList<Point>();
    connectedPoints.addAll(super.connectedToPoints());
    connectedPoints.add(primary);
    return connectedPoints;
  }

  @Override
  protected int connectedPoints() {
    return super.connectedPoints() + (primary == null ? 0 : 1);
  }

  @Override
  public String toString() {
    return super.toString() + " primary: " + primary.toString();
  }
}
