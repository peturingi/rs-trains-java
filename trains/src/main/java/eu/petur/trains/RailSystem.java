package eu.petur.trains;

import org.apache.commons.lang3.Validate;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URL;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 */
public final class RailSystem {
  final Set<Track> tracks;
  final Route route;

  private final static Logger logger = Logger.getLogger(RailSystem.class.getPackage().getName());

  /**
   * Class constructor for a RailSystem.
   *
   * @param networkConfiguration file containing the configuration for this systems network.
   * @param routeConfiguration file containing the configuration for this systems routes.
   * @throws FileNotFoundException network or configuration file was not found.
   * @throws IOException error while reading configuration files.
   * @throws IllegalStateException found an <code>End</code> which is not connected to a single point.
   * @throws IllegalStateException found a <code>Switch</code> which does not connect three points.
   * @throws IllegalStateException found a <code>Track</code> which does not connect two points.
   * @throws IllegalStateException the routeConfiguration does not have a station as its first point.
   * @throws IllegalStateException the route makes a jump on the tracks.
   */
  public RailSystem(final URL networkConfiguration, final URL routeConfiguration) throws IOException {
    logger.log(Level.FINE, "Building tracks network.");
    final Set<Track> tracks = NetworkBuilder.build(networkConfiguration);
    logger.log(Level.FINER, "Network build complete.");
    final int numberOfTracks = tracks.size();
    logger.log(Level.INFO, "Number of tracks in network: " + numberOfTracks);
    logger.log(Level.FINE, "Building route.");
    final Route route = TrainRouteBuilder.build(routeConfiguration);
    logger.log(Level.FINER, "Route build complete.");
    logger.log(Level.FINE, "Checking track connections.");
    checkTrackConnections(tracks);
    logger.log(Level.FINER, "Track connections check complete.");
    logger.log(Level.FINE, "Checking first point on route.");
    checkFirstPointOnRoute(route, tracks);
    logger.log(Level.FINER, "First point check complete");

    final int routeLength = getRouteSize(route);

    if (routeLength > 1) { /* Trivial case if [0;1]. */
      logger.log(Level.INFO, "Route length: " + routeLength);
      logger.log(Level.FINE, "Checking route completeness.");
      checkRouteCompleteness(tracks, route);
      logger.log(Level.FINER, "Route completeness check finished.");
    } else {
      logger.log(Level.WARNING, "Route length is 0 or 1.");
    }
    
    logger.log(Level.FINE, "Checking route validity.");
    checkRoute(route, tracks);
    logger.log(Level.FINER, "Route validity check complete.");

    logger.log(Level.INFO, "All rail system checks complete.");

    this.tracks = tracks;
    this.route = route;
  }

  /**
   * Ensure that tracks are always correctly connected to other tracks.
   *
   * @param tracks the tracks network
   * @throws IllegalStateException in case of a wrong track connection
   */
  private static void checkTrackConnections(Set<Track> tracks) throws IllegalStateException {
    for (Track t: tracks) {
      logger.log(Level.FINEST, "Validating track" + t.toString());
      validateTrack(t);
    }
  }

  /**
   * Ensure first point on a route is a station.
   *
   * @param route the proposed route
   * @param tracks the tracks network
   * @throws IllegalStateException if first point on the route is not a station
   */
  private static void checkFirstPointOnRoute(final Route route, final Set<Track> tracks) throws IllegalStateException {
    if (routeHasWaypoints(route)) {
      final Waypoint firstPoint = getWaypoint(route, 0);
      if (firstPoint != null) {
        Station firstStation = null;
        for (Track t : tracks) {
          if (t instanceof Station) {
            if (((Station) t).isAtPointWithId(firstPoint.getId())) {
              firstStation = (Station) t;
              break;
            }
          }
        }
        if (firstStation == null) {
          throw new IllegalStateException("There must be a station at the first point in a route.");
        }
      }
    }
  }

  /**
   * Check that train routes are complete in that they mention all tracks used
   * (error in specification, they must mention all points with an exception of the midpoint in a branch)
   * @param tracks the tracks network
   * @param route the proposed route
   */
  private static void checkRouteCompleteness(final Set<Track> tracks, final Route route) {
    final Iterator<Waypoint> waypointIterator = route.waypoints().iterator();
    Waypoint current = null;
    Waypoint next = waypointIterator.next();
    while (waypointIterator.hasNext()) {
      current = next;
      if (reachedEnd(waypointIterator)) {
        break;
      } else {
        next = waypointIterator.next();
      }
      checkTrackLeadTo(tracks, current, next);
    }
  }

  /**
   * Checks validity of a route.
   *
   * @param route the route to be checked
   * @param tracks the railway network
   */
  private static void checkRoute(final Route route, final Set<Track> tracks) throws IllegalStateException {
    logger.log(Level.FINEST, "Extracting points from tracks network.");
    final Set<Point> points = extractPoints(tracks);
    for (int i = 1; i < getRouteSize(route) - 1; i++) {
      Point cameFrom = null;
      Point current = null;
      Point next = null;
      for (Point p: points) {
        if (p.getId().equals(getWaypoint(route, i - 1).getId())) {
          cameFrom = p;
        }
        if (p.getId().equals(getWaypoint(route, i).getId())) {
          current = p;
        }
        if (p.getId().equals(getWaypoint(route, i + 1).getId())) {
          next = p;
        }
      }

      logger.log(Level.FINEST, "Checking next step on route: " + " i: " + i + " cameFrom: " + cameFrom + " current: "+ current + " next: " + next);
      ensurePossibilityOfSwitchCheck(cameFrom, current, next);
      performNextStep(route, i, cameFrom, current, next);
    }
  }

  /**
   * Performs a check for the next step on a route.
   *
   * @param route the route being checked
   * @param i the current iteration of th
   * @param cameFrom the previous point on the route
   * @param current the current point on the route
   * @param next the next point on the route
   * @throws IllegalStateException if it is not possible to travel as specified by parameters
   */
  private static void performNextStep(final Route route, final int i, final Point cameFrom, final Point current, final Point next) throws IllegalStateException {
    final Set<Track> tracksBetweenCameFromAndCurrent = findTracksBetweenTwoPoints(cameFrom, current);
    final Set<Track> tracksBetweenCurrentAndNext = findTracksBetweenTwoPoints(current, next);

    // current can not go back to cameFrom, if cameFrom is not stoppable,
    // unless there is more than 1 track leading from current to cameFrom
    if (tracksBetweenCameFromAndCurrent.size() == 1 && tracksBetweenCurrentAndNext.size() == 1) {
      if (cameFrom.getId().equals(next.getId())) {
        if (!getWaypoint(route, i).canStop()) {
          throw new IllegalStateException("Can not travel back to previous location as train can not stop at " + current.getId());
        }
      }
      checkTrackNotUsedWithoutStopping(route, i, tracksBetweenCameFromAndCurrent, tracksBetweenCurrentAndNext);
    }
  }

  /**
   *  See if same track is being used immediately again without stopping.
   *
   *  @param route the route being checked
   *  @param i the current index
   *  @param tracksBetweenCameFromAndCurrent the set of tracks between previous and current point
   *  @param tracksBetweenCurrentAndNext the set of tracks between current and next point
   *  @throws IllegalStateException on attempt to go back using same track without stopping
   */
  private static void checkTrackNotUsedWithoutStopping(final Route route, final int i, final Set<Track> tracksBetweenCameFromAndCurrent,
                                                       final Set<Track> tracksBetweenCurrentAndNext) throws IllegalStateException {
    if (union(tracksBetweenCameFromAndCurrent, tracksBetweenCurrentAndNext).size() == 1) {
      if (!getWaypoint(route, i).canStop()) {
        throw new IllegalStateException("Can not go immediately go back using same track without stopping.");
      }
    }
  }

  /**
   * Checks that a track conforms to the specifications.
   *
   * @param track the track to be checked
   * @throws IllegalStateException if the track does not conform to specifications
   */
  private static void validateTrack(final Track track) {
    if (track instanceof End) {
      if (track.connectedPoints() != 1) {
        throw new IllegalStateException("An end must be connected to one point.");
      }
    }
    else if (track instanceof Switch) {
      if (track.connectedPoints() != 3) {
        throw new IllegalStateException("A switch must connect three points.");
      }
    }
    else if (track.connectedPoints() != 2) {
      throw new IllegalStateException("A connection must be between two points.");
    }
  }

  /**
   * Get all tracks connected to same point as current(waypoint).
   *
   * @param tracks the tracks network
   * @param current the current waypoint
   * @return set of tracks connected to same point as current(waypoint)
   */
  private static Set<Track> getTracksTo(final Set<Track> tracks, final Waypoint current) {
    final Set<Track> leadingTracks = new HashSet<Track>();
    for (Track t: tracks) {
      for (Point tp : t.connectedToPoints()) {
        if (tp.getId().equals(current.getId())) {
          leadingTracks.add(t);
        }
      }
    }
    return leadingTracks;
  }

  /**
   * Ensure that any of the tracks lead to next(waypoint).
   *
   * @param tracks a tracks network
   * @param current the current waypoint
   * @param next the next waypoint
   * @throws IllegalStateException if no tracks lead to the next waypoint
   */
  private static void checkTrackLeadTo(final Set<Track> tracks, final Waypoint current, final Waypoint next) throws IllegalStateException {
    final Set<Track> leadingTracks = getTracksTo(tracks, current);
    boolean leadTo = false;
    for (Track l: leadingTracks) {
      if (checkTrackLeads(l, current, next)) {
        leadTo = true;
      }
    }
    if (!leadTo) {
      throw new IllegalStateException(current.getId() + " is not connected to " + next.getId());
    }
  }

  /**
   * Extract all points from train network.
   *
   * @param tracks a tracks network
   * @return points in the railway
   */
  private static Set<Point> extractPoints(final Set<Track> tracks) {
    final Set<Point> points = new HashSet<Point>();
    for (Track t: tracks) {
      points.addAll(t.connectedToPoints());
    }
    return points;
  }

  /**
   * Finds tracks from one point to another.
   *
   * @param p1 the origin point
   * @param p2 the destination point
   * @return a set of found tracks
   */
  private static Set<Track> findTracksBetweenTwoPoints(final Point p1, final Point p2) {
    final Set<Track> tracksBetweenTwoPoints = new HashSet<Track>(p1.getConnectedTracks());
    tracksBetweenTwoPoints.retainAll(p2.getConnectedTracks());
    final Iterator<Track> iterator = tracksBetweenTwoPoints.iterator();
    while (iterator.hasNext()) {
      final Track currentTrack = iterator.next();
      if (!checkTrackLeads(currentTrack, p1, p2)) {
        iterator.remove();
      }
    }
    return tracksBetweenTwoPoints;
  }

  /**
   * Check if a track leads between two points.
   *
   * @param track the track to be checked
   * @param p1 the first point
   * @param p2 the second point
   * @return whether or not a track leads from point p1 to p2
   */
  private static boolean checkTrackLeads(Track track, Point p1, Point p2) {
    return track.leads(p1.getId(), p2.getId());
  }

  /**
   * Ensures that it is possible to check a switch condition.
   *
   * @param cameFrom the previous point on the route
   * @param current the current point
   * @param next the next point on the route
   * @throws IllegalStateException if any of the points is null
   */
  private static void ensurePossibilityOfSwitchCheck(final Point cameFrom, final Point current, final Point next) throws IllegalStateException {
    if (cameFrom == null || current == null || next == null) {
      throw new IllegalStateException("Could not check if a switch condition is being violated.");
    }
  }

  /**
   * Get waypoint on a route by index.
   *
   * @param route the route
   * @param i index
   * @return the i'th waypoint on the route
   */
  private static Waypoint getWaypoint(final Route route, final int i) {
    return route.waypoints().get(i);
  }

  /**
   * Find the length of a route.
   *
   * @param route the route to be checked
   * @return the number of waypoints on the route
   */
  private static int getRouteSize(final Route route) {
    return route.waypoints().size();
  }

  /**
   * Verify that a route has waypoints
   *
   * @param route the route to be checked
   * @return whether or not the number the route contains any waypoints
   */
  private static boolean routeHasWaypoints(final Route route) {
    return getRouteSize(route) > 0;
  }

  /**
   * See if there are more points to check
   *
   * @param waypointIterator iterator over waypoints on a route
   * @return whether or not the iterator has reached the end
   */
  private static boolean reachedEnd(final Iterator<Waypoint> waypointIterator) {
    return !waypointIterator.hasNext();
  }

  /**
   * Get the union of two sets.
   *
   * @param a first set.
   * @param b second set.
   * @return the union of the two sets.
   * @throws NullPointerException either a or b is null.
   * @throws IllegalArgumentException if the union was not performed correctly.
   */
  private static Set<Track> union(final Set<Track> a, final Set<Track> b) {
    Validate.notNull(a, "The set cannot be null.");
    Validate.notNull(b, "The set cannot be null.");
    if (a == b)
      return a;
    final Set<Track> union = new HashSet<Track>(a);
    union.addAll(b);
    for(Track t : a) {
    	Validate.isTrue(union.contains(t), "Union was performed incorrectly");
    }
    for(Track t : b) {
      Validate.isTrue(union.contains(t), "Union was performed incorrectly");
    }
    return union;
  }
}