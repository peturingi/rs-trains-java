package eu.petur.trains.gen.TrainRoutes; // Generated from /Users/petur/DTU/RS/java-code/trains/src/main/java/eu/petur/trains/grammar/TrainRoutes.g4 by ANTLR 4.5
import org.antlr.v4.runtime.misc.NotNull;
import org.antlr.v4.runtime.tree.AbstractParseTreeVisitor;

/**
 * This class provides an empty implementation of {@link TrainRoutesVisitor},
 * which can be extended to create a visitor which only needs to handle a subset
 * of the available methods.
 *
 * @param <T> The return type of the visit operation. Use {@link Void} for
 * operations with no return type.
 */
public class TrainRoutesBaseVisitor<T> extends AbstractParseTreeVisitor<T> implements TrainRoutesVisitor<T> {
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	public T visitRoutes(@NotNull TrainRoutesParser.RoutesContext ctx) { return visitChildren(ctx); }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	public T visitStop(@NotNull TrainRoutesParser.StopContext ctx) { return visitChildren(ctx); }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	public T visitId(@NotNull TrainRoutesParser.IdContext ctx) { return visitChildren(ctx); }
}